package com.jbr450.customeraccountapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class CustomerController {
    @GetMapping("/accounts")
    public static ArrayList<Account> getAccountList() {
        Customer customer1 = new Customer(1, "Lan", 15);
        Customer customer2 = new Customer(2, "Nam", 20);
        Customer customer3 = new Customer(3, "Hoa", 10);
        System.out.println("customer1 là: " + customer1.toString());
        System.out.println("customer2 là: " + customer2.toString());
        System.out.println("customer3 là: " + customer3.toString());
        Account account1 = new Account(10, customer1, 10000);
        Account account2 = new Account(20, customer2, 20000);
        Account account3 = new Account(30, customer3, 15000);
        System.out.println("account1 là: " + account1.toString());
        System.out.println("account2 là: " + account2.toString());
        System.out.println("account3 là: " + account3.toString());
        ArrayList<Account> accountList = new ArrayList<Account>();
        accountList.add(account1);
        accountList.add(account2);
        accountList.add(account3);
        return accountList;
    }
}
